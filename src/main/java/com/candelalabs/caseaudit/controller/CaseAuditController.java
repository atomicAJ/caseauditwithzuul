package com.candelalabs.caseaudit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.candelalabs.caseaudit.model.CaseAuditFilter;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;
import com.candelalabs.caseaudit.service.CaseAuditService;

@RestController
public class CaseAuditController {

	@Autowired
	private CaseAuditService caseAuditService;

	private static final String ERROR = "No results obtanied";
	private static final String SEARCHCASEID = "caseId:";
	private static final String SEARCHAPPEND = "caseinfo.";

	@GetMapping("/getCaseAudit/{caseId}")
	public ResponseEntity<String> getCaseAuditData(@PathVariable("caseId") String caseId) {
		List<Object> response = caseAuditService.fetchCaseAuditOnSearch(SEARCHCASEID, caseId);
		if (null != response && !response.isEmpty()) {
			return new ResponseEntity<>(response.toString(), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(ERROR, HttpStatus.NO_CONTENT);
	}

	@PostMapping("/saveCaseAudit")
	public ResponseEntity<String> saveCaseAuditData(@RequestBody CaseAuditRequestBean caseAudit) {
		String response = caseAuditService.storeCaseAudit(caseAudit);
		if (null != response && !response.isEmpty()) {
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(ERROR, HttpStatus.NO_CONTENT);
	}

	@PostMapping("/getCaseAudit/filter/{caseId}")
	public ResponseEntity<String> filterCaseAudit(@RequestBody CaseAuditFilter filter,
			@PathVariable("caseId") String caseId) {
		List<Object> response = caseAuditService.fetchCaseAuditOnFilter(filter, caseId);
		if (null != response && !response.isEmpty()) {
			return new ResponseEntity<>(response.toString(), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(ERROR, HttpStatus.NO_CONTENT);
	}

	@GetMapping("/getCaseAudit/search/{field}/{value}")
	public ResponseEntity<String> searchCaseAudit(@PathVariable("field") String field,
			@PathVariable("value") String value) {
		List<Object> response = caseAuditService.fetchCaseAuditOnSearch(SEARCHAPPEND + field, value);
		if (null != response && !response.isEmpty()) {
			return new ResponseEntity<>(response.toString(), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(ERROR, HttpStatus.NO_CONTENT);
	}

}
