package com.candelalabs.caseaudit.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.candelalabs.caseaudit.util.CaseAuditUtilServices;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RefreshScope
@RestController
public class EventsRestController {

	@Autowired
	private CaseAuditUtilServices utilService;

	@Autowired
	ProducerTemplate producerTemplate;

	Map<String, Object> resultMap;

	private static final Logger LOGGER = LoggerFactory.getLogger(EventsRestController.class);
	private static final String KAFKASEND = "Message sent to the Kafka Topic Successfully";
	private static final String EVENT = "event";

	ObjectMapper objectMapper = new ObjectMapper();

	@PostMapping(path = "/logevent")
	public void sendEventToKafka(@RequestBody String checkoutEventJson) throws IOException {

		LOGGER.info("****** Entered Audit MicroService ******* {}", checkoutEventJson);

		Map<String, Object> map = objectMapper.readValue(checkoutEventJson, new TypeReference<Map<String, Object>>() {
		});
		String event = (String) map.get(EVENT);
		if (event != null)
			resultMap = utilService.getEventFields(event, map);

		LOGGER.debug(" @@@@@@@@@@  Event Data to be audited  @@@@@@@@@ {}", resultMap);

		JSONObject data = new JSONObject(resultMap);
		utilService.mapAuditData(data);

	}

}
