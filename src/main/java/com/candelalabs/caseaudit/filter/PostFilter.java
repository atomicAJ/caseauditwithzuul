package com.candelalabs.caseaudit.filter;

import com.netflix.zuul.ZuulFilter;

public class PostFilter extends ZuulFilter {

	private static final String POST = "post";

	@Override
	public String filterType() {
		return POST;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		System.out.println("Inside Response Filter");
		return null;
	}

}