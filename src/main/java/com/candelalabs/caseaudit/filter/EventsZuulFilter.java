package com.candelalabs.caseaudit.filter;

import java.io.InputStream;
import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletRequest;
import com.netflix.zuul.context.RequestContext;
import com.candelalabs.caseaudit.util.CaseAuditUtilServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

@Component
public class EventsZuulFilter extends ZuulFilter {

	public static final Logger LOGGER = LoggerFactory.getLogger(EventsZuulFilter.class);

	private static final String EVENT = "event";
	private static final String REQUESTURI = "requestURI";
	private static final String BACKSLASH = "/";
	private static final String UTF = "UTF-8";

	HttpURLConnection connection = null;
	String body;
	
	@Autowired
	private CaseAuditUtilServices utilService;

	@Value(value = "${event.AuditUrl}")
	private String eventURL;

	@Override
	public String filterType() {
		return "route";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public boolean shouldFilter() {
		return RequestContext.getCurrentContext().getRouteHost() != null
				&& RequestContext.getCurrentContext().sendZuulResponse();
	}

	@Override
	public Object run() {

		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();
		InputStream inputStream = null;
		JSONObject requestData = null;
		try {
			inputStream = request.getInputStream();
		} catch (IOException e1) {
			LOGGER.error("Request requestBody  inputStream+++  ", e1);
		}
		try {
			body = StreamUtils.copyToString(inputStream, Charset.forName(UTF));
			requestData = new JSONObject(body);
			String event=((context.get(REQUESTURI)).toString()).replaceAll(BACKSLASH, "");
			requestData.put(EVENT, utilService.getEventType(event));
		} catch (Exception e) {
			LOGGER.error("Exception in Zuul", e);
		}
		try {
			RestTemplate restTemplate = new RestTemplate();
			String responseCode = restTemplate.postForObject(eventURL, requestData.toString(), String.class);
			LOGGER.info("Sent to event URL : {}", responseCode);
		} catch (Exception e) {
			LOGGER.error("Inside Route Fileter", e);
		}
		return null;
	}
}
