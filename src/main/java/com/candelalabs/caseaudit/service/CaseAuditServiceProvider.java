package com.candelalabs.caseaudit.service;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.candelalabs.caseaudit.dao.CaseAuditDBService;
import com.candelalabs.caseaudit.model.CaseAuditFilter;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;
import com.candelalabs.caseaudit.model.CaseAuditResponseBean;
import com.candelalabs.caseaudit.util.CaseAuditUtilServices;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CaseAuditServiceProvider implements CaseAuditService {

	@Value(value = "${elasticsearch.restUrl}")
	private String elasticSearchURL;

	@Value(value = "${candela.events.dbSave}")
	private Boolean saveToDB;
	
	@Value(value = "${candela.events.elasticSave}")
	private Boolean saveToElastic;

	@Value(value = "${elasticsearch.result.size:#{10000}}")
	private String searchResultSize;

	@Autowired
	private CaseAuditDBService caseAuditDBService;

	@Autowired
	private CaseAuditUtilServices utilService;

	private static final String RESULT_SIZE = "\"size\":";
	private static final String START_PARANTHESISS = "{";
	private static final String QUERY_INIT = "\"query\":";
	private static final String QUERY_CLOSE = "}";
	private static final String COMMA = ",";
	private static final String COLON = ":,";

	private static final String USER = "user";
	private static final String ACTIVITYNAME = "activityName";
	private static final String PROCESSNAME = "processName";
	private static final String STARTTIME = "startTime";
	private static final String KEYWORD = ".keyword";
	private static final String CASEAUDITINSERTIONTIME = "caseAuditInsertionTime";
	private static final String CASEID = "caseId";

	private static final String SEARCH = "_search";
	private static final String SATTRIBUTE = "?q=";
	private static final String SEARCHCASEID = "caseId:";
	private static final String HITS = "hits";
	private static final String SOURCE = "_source";
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";

	public static final Logger LOGGER = LoggerFactory.getLogger(CaseAuditServiceProvider.class);

	/** Method to get Case Audit Data based on Case ID **/
	@Override
	public List<Object> getCaseAuditData(String caseId) {
		RestTemplate restTemplate = new RestTemplate();
		String elasticURL = elasticSearchURL + SEARCH + SATTRIBUTE + SEARCHCASEID + caseId;
		String jsnString = (restTemplate.getForEntity(elasticURL, String.class)).getBody();
		return parseJSONString(jsnString);
	}

	/** Method to Store Case Audit Data to DB and Elastic **/
	@Override
	public String storeCaseAudit(CaseAuditRequestBean caseAudit) {
		try {
			String caseId = caseAudit.getCaseId().toString();
			String dbResponse=new String();
				caseAudit.setCaseAuditInsertionTime(new Timestamp(System.currentTimeMillis()).toString());
				LOGGER.info("Saving Case Audit info to DB and Elastic Case ID: {}", caseId);
				if (saveToDB) {
					LOGGER.info("Saving Case Audit info to DB Task ID:{} ", caseAudit.getTaskId());
					dbResponse = caseAuditDBService.saveCaseAuditData(caseAudit);
					LOGGER.info("Saving Case Audit info to DB status: {}", dbResponse);
				}
				if (saveToElastic) {
					RestTemplate restTemplate = new RestTemplate();
					CaseAuditResponseBean caseAuditData = mapCaseAuditReqRes(caseAudit);
					if (dbResponse.equalsIgnoreCase(SUCCESS)) {
						LOGGER.info("Saving Case Audit info to Elastic Search");
						String elasticResponse = restTemplate.postForObject(elasticSearchURL, caseAuditData,
								String.class);
						LOGGER.info("Saving Case Audit info to Elastic Search status: {}", elasticResponse);
						return elasticResponse;
					}
				}
				LOGGER.info(dbResponse);
			return dbResponse;
		} catch (Exception e) {
			LOGGER.error("Error while saving case audit :", e);
			return ERROR;
		}
	}

	/**
	 * Method to map Case Audit Data from Request to Response to be saved in Elastic
	 * Search
	 **/
	public CaseAuditResponseBean mapCaseAuditReqRes(CaseAuditRequestBean caseAuditData) {
		CaseAuditResponseBean caseAudit = new CaseAuditResponseBean();
		caseAudit.setCaseId(caseAuditData.getCaseId());
		caseAudit.setActivityName(caseAuditData.getActivityName());
		caseAudit.setCaseAuditInsertionTime(caseAuditData.getCaseAuditInsertionTime());
		caseAudit.setCaseinfo(caseAuditData.getCaseinfo());
		caseAudit.setEvent(caseAuditData.getEvent());
		caseAudit.setProcessInstanceName(caseAuditData.getProcessInstanceName());
		caseAudit.setProcessName(caseAuditData.getProcessName());
		caseAudit.setUser(caseAuditData.getUser());
		caseAudit.setEventDetails(utilService.getEventMessage(caseAuditData));
		return caseAudit;
	}

	/** Method to parse Elastic Document to Case Audit JSON **/
	public List<Object> parseJSONString(String jsonString) {
		List<Object> response = new ArrayList<>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			JSONArray jArray = (jsonObj.getJSONObject(HITS)).getJSONArray(HITS);
			for (Object data : jArray) {
				response.add(((JSONObject) data).getJSONObject(SOURCE));
			}
		} catch (Exception e) {
			LOGGER.error("Json data unparsable {}", e);
		}
		return response;

	}

	/** Method to get Case Audit Data Based on the Filter **/
	public List<Object> fetchCaseAuditOnFilter(CaseAuditFilter filter, String caseId) {
		List<Object> response = null;
		StringBuilder queryBuilder = new StringBuilder();
		Map<String, Object> filterMap = new HashMap<>();
		queryBuilder.append(START_PARANTHESISS);
		queryBuilder.append(RESULT_SIZE + searchResultSize + COMMA);
		queryBuilder.append(QUERY_INIT);

		Field[] fields = filter.getClass().getDeclaredFields();
		Object cast2obj = filter;
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				Object fileldValue = field.get(cast2obj);
				if (null != fileldValue) {
					filterMap.put(field.getName(), fileldValue);
				}
			} catch (Exception e) {
				LOGGER.error("Unable to validate the field {}", e);
			}
		}
		BoolQueryBuilder queryBuilderImpl = QueryBuilders.boolQuery();
		queryBuilderImpl.must(QueryBuilders.termsQuery(CASEID, caseId));
		filterMap.forEach((key, value) -> {
			if (key.equalsIgnoreCase(USER)) {
				queryBuilderImpl.must(QueryBuilders.termsQuery(USER, filter.getUser()));
			}
			if (key.equalsIgnoreCase(ACTIVITYNAME)) {
				queryBuilderImpl.filter(QueryBuilders.termsQuery(ACTIVITYNAME + KEYWORD, filter.getActivityName()));
			}
			if (key.equalsIgnoreCase(STARTTIME)) {
				queryBuilderImpl.should(QueryBuilders.rangeQuery(CASEAUDITINSERTIONTIME + KEYWORD)
						.from(filter.getStartTime()).to(filter.getEndTime())).minimumShouldMatch(1);

			}
			if (key.equalsIgnoreCase(PROCESSNAME)) {
				queryBuilderImpl.filter(QueryBuilders.termsQuery(PROCESSNAME + KEYWORD, filter.getProcessName()));
			}
		});
		queryBuilder.append(queryBuilderImpl);
		queryBuilder.append(QUERY_CLOSE);
		response = fetchComments(elasticSearchURL, null, queryBuilder.toString());
		return response;
	}

	/** Method connect to Elastic server for filter data **/
	public List<Object> fetchComments(String elasticSearchURL, String processId, String query) {
		RestTemplate restTemplate = new RestTemplate();
		elasticSearchURL = elasticSearchURL + SEARCH;
		List<Object> response = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(query, headers);
		String result = restTemplate.postForObject(elasticSearchURL, entity, String.class);
		response = parseJSONString(result);
		return response;
	}

	/** Method to get Case Audit Data Based on the Search **/
	public List<Object> fetchCaseAuditOnSearch(String field, String value) {
		RestTemplate restTemplate = new RestTemplate();
		String elasticURL = elasticSearchURL + SEARCH + SATTRIBUTE + field + COLON + value;
		String jsnString = (restTemplate.getForEntity(elasticURL, String.class)).getBody();
		return parseJSONString(jsnString);
	}

}
