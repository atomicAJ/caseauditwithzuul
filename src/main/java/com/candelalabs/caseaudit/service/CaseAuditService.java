package com.candelalabs.caseaudit.service;

import java.util.List;

import com.candelalabs.caseaudit.model.CaseAuditFilter;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;


public interface CaseAuditService {
	
	public String storeCaseAudit(CaseAuditRequestBean caseAudit);
	
	public List<Object> getCaseAuditData(String caseId);
	
	public List<Object> fetchCaseAuditOnFilter(CaseAuditFilter filter, String caseId);
	
	public List<Object> fetchCaseAuditOnSearch(String field,String value);

}
