package com.candelalabs.caseaudit.dao;

import javax.persistence.*;

@Table(name = "FFCaseData")
@Entity
public class FFCaseData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	private String caseData;

	@Column(name = "case_id")
	private Integer caseId;

	public Integer getCaseId() {
		return caseId;
	}

	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public FFCaseData() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCaseData() {
		return caseData;
	}

	public void setCaseData(String caseData) {
		this.caseData = caseData;
	}
}