package com.candelalabs.caseaudit.dao;

import com.candelalabs.caseaudit.model.CaseAuditRequestBean;

public interface CaseAuditDBService {

	public String saveCaseAuditData(CaseAuditRequestBean caseAudit);
	
	public String fetchCaseData(String caseId);

}
