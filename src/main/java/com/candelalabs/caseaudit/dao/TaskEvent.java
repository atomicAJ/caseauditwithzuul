package com.candelalabs.caseaudit.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "caseinfo", "processName","activityName", "eventDetails","caseId","user","processInstanceName"})
@Entity
@Table(name = "TASKEVENT")
public class TaskEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "TASKID")
	private Long taskId;

	@Column(name = "CASEAUDITINSERTIONTIME")
	private String caseAuditInsertionTime;

	@Column(name = "EVENT")
	private String event;

	@Column(name = "FROMUSER")
	private String fromUser;

	@Column(name = "TOUSER")
	private String toUser;
	
	@Column(name = "DECISIONNAME")
	private String decisionName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDecisionName() {
		return decisionName;
	}

	public void setDecisionName(String decisionName) {
		this.decisionName = decisionName;
	}

	public String getCaseAuditInsertionTime() {
		return caseAuditInsertionTime;
	}

	public void setCaseAuditInsertionTime(String caseAuditInsertionTime) {
		this.caseAuditInsertionTime = caseAuditInsertionTime;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

}
