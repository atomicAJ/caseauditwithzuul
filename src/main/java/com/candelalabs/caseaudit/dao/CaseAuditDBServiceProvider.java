package com.candelalabs.caseaudit.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.candelalabs.caseaudit.model.CaseAuditRequestBean;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class CaseAuditDBServiceProvider implements CaseAuditDBService {

	public static final Logger LOGGER = LoggerFactory.getLogger(CaseAuditDBServiceProvider.class);

	private static final String SUCCESS = "success";
	private static final String ERROR = "Error while saving to DB";

	@Autowired
	@PersistenceContext
	private EntityManager entityManager;

	ObjectMapper objectMapper = new ObjectMapper();

	/** Method to save Case Audit Data to DB **/
	@Override
	@Transactional
	public String saveCaseAuditData(CaseAuditRequestBean caseAuditData) {
		try {
			TaskEvent taskEvent = mapCaseAuditData(caseAuditData);
			if (null != taskEvent) {
				entityManager.persist(taskEvent);
				LOGGER.info("Case Audit data saved");
				return SUCCESS;
			}
		} catch (Exception e) {
			LOGGER.error("Error while saving Case Audit to DB:", e);
			return ERROR + " " + e.getMessage();
		}
		return ERROR;
	}

	/** Method to map Case Audit Request to Task Event **/
	public TaskEvent mapCaseAuditData(CaseAuditRequestBean caseAuditData) {
		TaskEvent taskEvent = new TaskEvent();
		JSONObject eventDetails = new JSONObject(caseAuditData.getEventDetails());
		try {
			taskEvent = objectMapper.readValue(eventDetails.toString(), TaskEvent.class);
			taskEvent.setEvent(caseAuditData.getEvent());
			taskEvent.setTaskId(caseAuditData.getTaskId());
			taskEvent.setCaseAuditInsertionTime(caseAuditData.getCaseAuditInsertionTime());
			return taskEvent;
		} catch (Exception e) {
			LOGGER.error("Json data unparsable {}", e);
		}
		return null;
	}

	/** Method to fetch case Data from DB **/
	public String fetchCaseData(String caseId) {
		try {
		TypedQuery<String> tagQuery = (TypedQuery<String>) entityManager
				.createQuery("Select caseData from FFCaseData where caseId =:caseId");
		tagQuery.setParameter("caseId", Integer.valueOf(caseId));
		List<String> result = tagQuery.getResultList();
		JSONObject response = new JSONObject(result.get(0));
		return response.toString();
		}
		catch(Exception e)
		{
			LOGGER.error("Error getting case data for case ID: {}",caseId,e);
			return null;
		}
	}

}
