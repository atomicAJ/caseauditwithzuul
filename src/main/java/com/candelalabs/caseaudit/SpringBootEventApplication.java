package com.candelalabs.caseaudit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import com.candelalabs.caseaudit.filter.EventsZuulFilter;

@EnableZuulProxy
@SpringBootApplication
@ImportResource("classpath:camel-route.xml")
public class SpringBootEventApplication {

	public static void main(String[] args) {

		SpringApplication.run( SpringBootEventApplication.class , args);
	}

	@Bean
	public EventsZuulFilter eventsZuulFilter() {
		return new EventsZuulFilter();
	}

}