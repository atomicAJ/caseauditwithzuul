/**
 * 
 */
package com.candelalabs.caseaudit.model;

import java.util.List;

public class CaseAuditFilter
{

	private List<String> user;
	
	private List<String> activityName;
	
	private String startTime;
	
	private String endTime;
	
	private List<String> processName;
	
	public List<String> getActivityName()
	{
		return activityName;
	}

	public void setActivityName(List<String> activityName)
	{
		this.activityName = activityName;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}

	public List<String> getUser() {
		return user;
	}

	public void setUser(List<String> user) {
		this.user = user;
	}

	public List<String> getProcessName() {
		return processName;
	}

	public void setProcessName(List<String> processName) {
		this.processName = processName;
	}
	
}
