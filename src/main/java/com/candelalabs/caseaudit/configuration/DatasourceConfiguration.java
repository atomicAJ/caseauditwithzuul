package com.candelalabs.caseaudit.configuration;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@Configuration
public class DatasourceConfiguration {

	@Value(value = "${spring.datasource.url}")
	String url;
	
	@Value(value = "${spring.datasource.username}")
	String uname;
	
	@Value(value = "${spring.datasource.password}")
	String password;
	
	 @Bean
	    @Primary
	    public DataSource hsqldbDataSource() throws SQLException {
	        final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
	        dataSource.setDriver(new org.postgresql.Driver());
	        dataSource.setUrl(url);
	        dataSource.setUsername(uname);
	        dataSource.setPassword(password);
	        return dataSource;
	    }

}
