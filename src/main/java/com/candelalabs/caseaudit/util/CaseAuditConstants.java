package com.candelalabs.caseaudit.util;

public class CaseAuditConstants {

	/** Events constants **/
	public static final String ASSIGN = "assign";
	public static final String CHECKOUT = "checkout";	
	public static final String DECISION = "decision";
	
	/** Event messages constants **/
	public static final String ASSIGNMSG = "Case assigned from %1$s to %2$s";
	public static final String CHECKOUTMSG = "Case has been cheked out";	
	public static final String DECISIONMSG = "Decision taken: %1$s";
	
	/** Task Details variables constants **/
	public static final String FROMUSER="fromUser";
	public static final String TOUSER="toUser";
	public static final String DECISIONNAME="decisionName";

}
