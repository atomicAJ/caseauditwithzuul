package com.candelalabs.caseaudit.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.camel.ProducerTemplate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import com.candelalabs.caseaudit.controller.EventsRestController;
import com.candelalabs.caseaudit.dao.CaseAuditDBService;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;
import com.fasterxml.jackson.databind.ObjectMapper;

@ConfigurationProperties(prefix = "candela.events")
@Service
public class CaseAuditUtilServices {

	@Autowired
	private CaseAuditDBService caseAuditDBService;

	@Autowired
	ProducerTemplate producerTemplate;

	/**
	 * Event related fields to be fetched from request, defined in properties file
	 **/
	private List<String> checkout;
	private List<String> decision;
	private List<String> auditconstants;
	private String taskPIDs;
	
	@Value(value = "${event.decisionName}")
	private static String decisionName;
	
	@Value(value = "${event.decisionValue}")
	private String decisionValue;
	
	@Value(value = "${event.checkoutName}")
	private String checkoutName;
	
	@Value(value = "${event.checkoutValue}")
	private String checkoutValue;

	private static final String CASEID = "caseId";
	private static final String TASKID = "taskId";
	private static final String CASEINFO = "caseinfo";
	private static final String SPILTON = "\\s*::\\s*";
	private static final String TPIDS = "taskPIDs";

	private static final Logger LOGGER = LoggerFactory.getLogger(EventsRestController.class);

	public List<String> getAuditconstants() {
		return auditconstants;
	}

	public void setAuditconstants(List<String> auditconstants) {
		this.auditconstants = auditconstants;
	}

	public List<String> getDecision() {
		return decision;
	}

	public void setDecision(List<String> decision) {
		this.decision = decision;
	}

	public List<String> getCheckout() {
		return checkout;
	}

	public void setCheckout(List<String> checkout) {
		this.checkout = checkout;
	}

	public String getTaskPIDs() {
		return taskPIDs;
	}

	public void setTaskPIDs(String taskPIDs) {
		this.taskPIDs = taskPIDs;
	}

	public String getDecisionName() {
		return decisionName;
	}

	public void setDecisionName(String decisionName) {
		this.decisionName = decisionName;
	}

	public String getCheckoutName() {
		return checkoutName;
	}

	public void setCheckoutName(String checkoutName) {
		this.checkoutName = checkoutName;
	}

	/** Method to get Audit message based on the audit action/task **/
	public String getEventMessage(CaseAuditRequestBean caseAuditData) {
		switch (caseAuditData.getEvent()) {
		case CaseAuditConstants.ASSIGN: {
			HashMap<String, String> actionParameters = (HashMap<String, String>) caseAuditData.getEventDetails();
			return String.format(CaseAuditConstants.ASSIGN, actionParameters.get(CaseAuditConstants.FROMUSER),
					actionParameters.get(CaseAuditConstants.TOUSER));
		}
		case CaseAuditConstants.DECISION: {
			HashMap<String, String> actionParameters = (HashMap<String, String>) caseAuditData.getEventDetails();
			return String.format(CaseAuditConstants.DECISIONMSG, actionParameters.get(CaseAuditConstants.DECISIONNAME));
		}
		default:
			return null;
		}
	}

	/** Method to get Audit fields based on action **/
	public Map<String, Object> getEventFields(String event, Map<String, Object> map) {
		Map<String, Object> resultMap;
		switch (event) {
		case CaseAuditConstants.CHECKOUT: {
			resultMap = getCheckout().stream().filter(map::containsKey)
					.collect(Collectors.toMap(Function.identity(), map::get));
			return resultMap;
		}
		case CaseAuditConstants.DECISION: {
			resultMap = getDecision().stream().filter(map::containsKey)
					.collect(Collectors.toMap(Function.identity(), map::get));
			return resultMap;
		}
		default:
			return null;
		}
	}

	public List<String> mapAuditData(JSONObject json) {
		List<String> auditData = new ArrayList<>();
		json.put("caseAuditInsertionTime", new Timestamp(System.currentTimeMillis()).toString());
		if (json.has(TPIDS)) {
			mapTaskPIDswithoutSpilt(json);
		} else {
			producerTemplate.sendBody("direct:camel", json.toString());
		}
		return auditData;
	}

	/** Method to transform request JSON to case Audit JSON using JOLT **/
	public void mapTaskPIDs(JSONObject json) {
		JSONArray taskDetails = json.getJSONArray(TPIDS);
		json.remove(TPIDS);
		List<String> fields = Arrays.asList(getTaskPIDs().split(SPILTON));
		try {
			for (int i = 0; i < taskDetails.length(); i++) {
				List<String> values = Arrays.asList(((taskDetails.get(i)).toString()).split(SPILTON));
				if (!values.isEmpty() && !fields.isEmpty() && values.size() == fields.size()) {
					for (int j = 0; j < fields.size(); j++) {
						if (fields.get(j).equals(CASEID)) {
							json.put(CASEINFO, new JSONObject(caseAuditDBService.fetchCaseData(values.get(j))));
						}
						json.put(fields.get(j), values.get(j));
					}
				}
				producerTemplate.sendBody("direct:camel", json.toString());
			}
		} catch (Exception e) {
			LOGGER.error("Error while parsiong the data {}", e.getMessage());
		}
	}
	
	public String addCaseInfo(String json)
	{
		JSONObject data= new JSONObject(json);
		data.put(CASEINFO, new JSONObject(caseAuditDBService.fetchCaseData(data.get(CASEID).toString())));
		return data.toString();
		
	}
	
	/** Method to transform request JSON without Spilt to case Audit JSON using JOLT **/
	public void mapTaskPIDswithoutSpilt(JSONObject json) {
		JSONArray taskDetails = json.getJSONArray(TPIDS);
		json.remove(TPIDS);
		try {
			for (int i = 0; i < taskDetails.length(); i++) {
						json.put(TPIDS, taskDetails.get(i));
				producerTemplate.sendBody("direct:camel", json.toString());
			}
		} catch (Exception e) {
			LOGGER.error("Error while parsiong the data {}", e.getMessage());
		}
	}
	
	public String getEventType(String data)
	{
		if(data.equals(decisionName))
		{
			return decisionValue;
		}
		else if(data.equals(checkoutName))
		{
			return checkoutValue;
		}
		return data;
	}

}
